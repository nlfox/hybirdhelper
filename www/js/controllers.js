var url = 'http://210.30.100.92:5000/';
angular.module('ionic.utils', [])

    .factory('$localstorage', ['$window', function ($window) {
        return {
            set: function (key, value) {
                $window.localStorage[key] = value;
            },
            get: function (key, defaultValue) {
                return $window.localStorage[key] || defaultValue;
            },
            setObject: function (key, value) {
                $window.localStorage[key] = JSON.stringify(value);
            },
            getObject: function (key) {
                return JSON.parse($window.localStorage[key] || '{}');
            },
            clear: function () {
                $window.localStorage.clear();
            },
            remove: function (key) {
                $window.localStorage[key] = null;
            }
        }
    }]);

angular.module('starter.controllers', ['ionic', 'ionic.utils'])

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout, $ionicLoading, $localstorage, $http, $rootScope, $ionicPopup) {
        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        //加载中
        $scope.loading = function () {
            $ionicLoading.show({
                content: 'Loading Data',
                animation: 'fade-in',
                showBackdrop: false,
                maxWidth: 200,
                showDelay: 500
            });
        };


        // Form data for the login modal
        $scope.loginData = {};
        $scope.regData = {};
        $rootScope.token = $localstorage.get('token');

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
            if (!$rootScope.token) {
                modal.show();
            }
        });
        $scope.showAlert = function (msg) {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: msg
            });
            alertPopup.then(function (res) {
                //console.log('Thank you for not eating my delicious ice cream cone');
            });
        };


        //注册modal
        $ionicModal.fromTemplateUrl('templates/modal/register-modal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.reg = modal;
        });


        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };
        // 开启注册框
        $scope.showReg = function () {
            $scope.reg.show();
        };
        // 关闭注册框
        $scope.closeReg = function () {
            $scope.reg.hide();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            //加载动画
            $scope.loading();
            console.log('Doing login', $scope.loginData);
            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $http.post(url + 'user/login', {
                username: $scope.loginData.username,
                password: $scope.loginData.password
            }).success(function (data, status) {
                console.log(data);
                $localstorage.set('token', data);
                $ionicLoading.hide();
                var token = data;
                $http.get(url + 'info/urpinfo?token=' + token).success(
                    function (data) {
                        if (data.code) {
                            var cname = data.message.cname;
                            $scope.showAlert('欢迎你 ' + cname);
                            $scope.cname = cname;
                            $localstorage.set('cname', cname);
                            if (data.message.location) {
                                $localstorage.set('location', data.message.location);
                            }
                            $ionicLoading.hide();
                        }
                        else {
                            $scope.showAlert('请先绑定教务账号');
                            $ionicLoading.hide();
                        }
                    }
                );
                $scope.closeLogin();
            });
        };

        $scope.doReg = function () {
            $scope.loading();
            console.log('Doing reg', $scope.loginData);

            $http.post(url + 'user/register', {
                username: $scope.regData.username,
                password: $scope.regData.password,
                email: $scope.regData.email
            }).success(function (data, status) {

                console.log(data);
                $ionicLoading.hide();
                if (data.code == 1) {
                    $scope.closeReg();
                }
            });
        };
    })


    .controller('BorrowCtrl', function ($localstorage, $scope, $http, $ionicLoading) {
        $scope.doRefresh = function () {
            $http.get(url + 'info/borrow?token=' + token).success(function (response) {
                $scope.borrow.result = response;
                if (response.code == 0) {
                    $scope.borrow.result = [["", "", "请先绑定图书馆账号"]]
                }
                $ionicLoading.hide();
                $scope.$broadcast('scroll.refreshComplete');
            });
        };
        $scope.borrow = {};
        var token = $localstorage.get('token');
        $ionicLoading.show();
        $http.get(url + 'info/borrow?token=' + token).success(function (response) {
            $scope.borrow.result = response;
            if (response.code == 0) {
                $scope.borrow.result = [["", "", "请先绑定图书馆账号"]]
            }
            $ionicLoading.hide();
        });
        $scope.search = {};
        $scope.doSearch = function () {
            $http.get(url + 'search/' + $scope.search.keyword).success(function (response) {
                $scope.search.result = response
            })
        };

    })


    .controller('SearchCtrl', function ($scope, $http) {
        $scope.search = {};
        $scope.doSearch = function () {
            $http.get(url + 'search/' + $scope.search.keyword).success(function (response) {
                $scope.search.result = response
            })
        };
    })

    .controller('SettingCtrl', function ($rootScope, $scope, $http, $ionicModal, $ionicLoading, $ionicPopup, $localstorage) {
        var token = $localstorage.get('token');
        $scope.accounts = {};
        $scope.cname = $localstorage.get('cname');
        $http.get(url + 'user/account?token=' + token).success(function (response) {
            $scope.accounts.list = response
        });
        $ionicModal.fromTemplateUrl('templates/modal/account-modal.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.addAcc = modal;
        });

        $scope.openAdd = function () {
            $scope.addAcc.show();
        };

        $scope.showAlert = function (msg) {
            var alertPopup = $ionicPopup.alert({
                title: 'Info',
                template: msg
            });
            alertPopup.then(function (res) {
                //console.log('Thank you for not eating my delicious ice cream cone');
            });
        };

        $scope.closeAdd = function () {
            $scope.addAcc.hide();
        };

        $scope.addAccount = function () {
            $ionicLoading.show();
            $http.post(url + 'user/account?token=' + token, {
                username: $scope.accounts.username,
                password: $scope.accounts.password,
                cms: $scope.accounts.cms
            }).success(function (data, status) {
                console.log(data);
                $http.get(url + 'info/' + $scope.accounts.cms + 'info?token=' + token).success(
                    function (data) {
                        if (data.code) {
                            var cname = data.message.cname;
                            $scope.showAlert('欢迎你 ' + cname);
                            $scope.cname = cname;
                            $localstorage.set('cname', cname);
                            if (data.message.location) {
                                $localstorage.set('location', data.message.location);
                            }
                            $ionicLoading.hide();
                            $scope.closeAdd();
                        }
                        else {
                            $scope.showAlert('账号或者密码错误!');
                            $ionicLoading.hide();
                        }
                    }
                );
            });
        };
        $scope.close = function () {
            localStorage.clear();
            $localstorage.clear();
        }

    })

    .controller('ScoresCtrl', function ($rootScope, $scope, $http, $localstorage) {
        var token = $localstorage.get('token');
        $scope.score = {};
        function getScore() {
            $http.get(url + 'info/score?token=' + $localstorage.get('token')).success(function (response) {
                $scope.score.list = response;

                if (response.code == 0) {
                    $scope.score.list = [["", "", "请登录教务账号"]]
                }
                $localstorage.setObject('score', response);
            });
        }

        $scope.doRefresh = function () {
            $http.get(url + 'info/score?token=' + $localstorage.get('token')).success(function (response) {
                $scope.score.list = response;
                if (response.code == 0) {
                    $scope.score.list = [["", "", "请登录教务账号"]]
                }
                $scope.$broadcast('scroll.refreshComplete');
            });
        };
        var score = $localstorage.getObject('score');

        if (typeof(score[0]) == "undefined") {
            console.log("get new data");
            console.log(score[0] == "undefined");
            getScore();
        } else {
            console.log("from localstorage");
            $scope.score.list = score;
        }
        $scope.doCalc = function () {
            var sum = 0;
            var totalClass = 0;
            var scores = [];
            console.log($scope.score.list);
            for (var t in $scope.score.list) {
                if ($scope.score.list[t][6] != "") {
                    totalClass++;
                    var row = [Number($scope.score.list[t][6]), Number($scope.score.list[t][4])];
                    scores.push(row);
                }
            }
            console.log(scores);
            var scoreSum = 0;
            var pointSum = 0;
            var weightAve = 0;
            for (t in scores) {
                scoreSum += scores[t][0];
                pointSum += scores[t][1];
            }
            for (t in scores) {
                weightAve += scores[t][0] * scores[t][1] / pointSum;
            }

            $scope.score.average = scoreSum / totalClass;
            $scope.score.weightAve = weightAve;
            $scope.score.gpa = weightAve / 25;
        }

    })

    .controller('BbsCtrl', function ($rootScope, $scope, $http, $localstorage, $ionicModal) {
        $scope.bbs = {};
        $scope.bbs.location = $localstorage.get('location');
        var token = $localstorage.get('token');
        var location = $scope.bbs.location;
        var by = function(name){
            return function(o, p){
                var a, b;
                if (typeof o === "object" && typeof p === "object" && o && p) {
                    a = o[name];
                    b = p[name];
                    if (a === b) {
                        return 0;
                    }
                    if (typeof a === typeof b) {
                        return a > b ? -1 : 1;
                    }
                    return typeof a > typeof b ? -1 : 1;
                }
                else {
                    throw ("error");
                }
            }
        };
        $ionicModal.fromTemplateUrl('/templates/modal/post-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.postModal = modal;
        });

        $scope.openPostModal = function () {
            $scope.postModal.show();
        };
        $scope.closePostModal = function () {
            $scope.postModal.hide();
        };
        $scope.addPost = function () {
            var title = $scope.bbs.title;
            var content = $scope.bbs.content;
            $http.post(url + 'bbs/' + location + '/?token=' + token, {
                title: title,
                content: content
            }).success(function (data, status) {
                $scope.bbs.posts = data;
                getPosts();
                if (data.code == 1) {
                    $scope.closePostModal();
                    //$scope.score.comment = "";
                }
            })

        };

        function getMyPosts() {
            $http.get(url + 'bbs/myposts/?token=' + token).success(function (response) {
                $scope.bbs.myposts = response.sort(by("id"));
                $localstorage.setObject('myposts', response.sort(by("id")));
            });
        }

        $scope.doMyRefresh = function () {

        };

        getMyPosts();

        function getPosts() {
            $http.get(url + 'bbs/' + location + '?token=' + token).success(function (response) {
                $scope.bbs.posts = response.sort(by("id"));
                $localstorage.setObject('posts', response.sort(by("id")));
            });
        }

        $scope.doRefresh = function () {
            $http.get(url + 'bbs/' + location + '?token=' + token).success(function (response) {
                $scope.bbs.posts = response.sort(by("id"));
                $localstorage.setObject('posts', response.sort(by("id")));
                //$scope.$broadcast('scroll.refreshComplete');
            });
            $http.get(url + 'bbs/myposts/?token=' + token).success(function (response) {
                $scope.bbs.myposts = response.sort(by("id"));
                $localstorage.setObject('myposts', response.sort(by("id")));
                $scope.$broadcast('scroll.refreshComplete');
            });

        };

        var posts = $localstorage.getObject('posts');

        if (typeof(posts[0]) == "undefined") {
            console.log("get new data");
            getPosts();
        } else {
            console.log("from localstorage");
            $scope.bbs.posts = posts;
        }

    })
    .controller('BbsPostCtrl', function ($localstorage, $scope, $stateParams, $http, $ionicModal, $ionicLoading) {
        console.log($stateParams);
        var token = $localstorage.get('token');
        var location = '35';
        $ionicModal.fromTemplateUrl('/templates/modal/comment-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });
        // Execute action on hide modal

        $scope.refreshScore = function () {
            getScore();
        };

        $scope.addComment = function () {

            $http.post(url + 'bbs/' + location + '/' + $scope.id + '/?token=' + token, {
                content: $scope.score.comment
            }).success(function (data, status) {
                $scope.score.tests = data;
                getComments();
                if (data.code == 1) {
                    $scope.closeModal();
                    $scope.score.comment = "";
                }
            })
        };
        var id = $stateParams.id;
        $scope.id = id;
        $scope.score = {};
        function getComments() {
            $ionicLoading.show();
            $http.get(url + 'bbs/' + location + '/' + id + '/?token=' + token).success(function (response) {
                console.log(response);
                $scope.score.list = response;
                $ionicLoading.hide();
            });
        }

        getComments();

    })
    //comments


    .controller('CommentsCtrl', function ($localstorage, $scope, $stateParams, $http, $ionicModal, $ionicLoading) {
        console.log($stateParams);
        var token = $localstorage.get('token');
        $ionicModal.fromTemplateUrl('/templates/modal/comment-modal.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function (modal) {
            $scope.modal = modal;
        });

        $scope.openModal = function () {
            $scope.modal.show();
        };
        $scope.closeModal = function () {
            $scope.modal.hide();
        };
        //Cleanup the modal when we're done with it!
        $scope.$on('$destroy', function () {
            $scope.modal.remove();
        });
        // Execute action on hide modal

        $scope.refreshScore = function () {
            getScore();
        };

        $scope.addComment = function () {

            $http.post(url + 'info/comments/' + $scope.id + '/?token=' + token, {
                content: $scope.score.comment
            }).success(function (data, status) {
                $scope.score.tests = data;
                getComments();
                if (data.code == 1) {
                    $scope.closeModal();
                    $scope.score.comment = "";
                }
            })
        };
        var id = $stateParams.courseId;
        $scope.id = id;
        $scope.score = {};
        function getComments() {
            $ionicLoading.show();
            $http.get(url + 'info/comments/' + id + '/?token=' + token).success(function (response) {
                console.log(response);
                $scope.score.list = response;
                $ionicLoading.hide();
            });
        }

        getComments();
    });

