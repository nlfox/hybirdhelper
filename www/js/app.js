angular.module('starter', ['ionic', 'starter.controllers'])

    .run(function ($ionicPlatform) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        $httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';

        /**
         * The workhorse; converts an object to x-www-form-urlencoded serialization.
         * @param {Object} obj
         * @return {String}
         */
        var param = function (obj) {
            var query = '', name, value, fullSubName, subName, subValue, innerObj, i;

            for (name in obj) {
                value = obj[name];

                if (value instanceof Array) {
                    for (i = 0; i < value.length; ++i) {
                        subValue = value[i];
                        fullSubName = name + '[' + i + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                }
                else if (value instanceof Object) {
                    for (subName in value) {
                        subValue = value[subName];
                        fullSubName = name + '[' + subName + ']';
                        innerObj = {};
                        innerObj[fullSubName] = subValue;
                        query += param(innerObj) + '&';
                    }
                }
                else if (value !== undefined && value !== null)
                    query += encodeURIComponent(name) + '=' + encodeURIComponent(value) + '&';
            }

            return query.length ? query.substr(0, query.length - 1) : query;
        };

        // Override $http service's default transformRequest
        $httpProvider.defaults.transformRequest = [function (data) {
            return angular.isObject(data) && String(data) !== '[object File]' ? param(data) : data;
        }];


        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "templates/menu.html",
                controller: 'AppCtrl'
            })

            .state('app.search', {
                url: "/search",
                views: {
                    'menuContent': {
                        templateUrl: "templates/search.html",
                        controller: "SearchCtrl"
                    }
                }


            })

            .state('app.browse', {
                url: "/browse",
                views: {
                    'menuContent': {
                        controller: 'BorrowCtrl',
                        templateUrl: "templates/browse.html"
                    }
                }
            })
            .state('app.scores', {
                url: "/scores",
                views: {
                    'menuContent': {
                        templateUrl: "templates/score.html",
                        controller: 'ScoresCtrl'
                    }
                }
            })
            .state('app.settings', {
                url: "/settings",
                views: {
                    'menuContent': {
                        controller: 'SettingCtrl',
                        templateUrl: "templates/settings.html"
                    }
                }
            })
            .state('app.bbs', {
                url: "/bbs",
                views: {
                    'menuContent': {
                        controller: 'BbsCtrl',
                        templateUrl: "templates/bbs.html"
                    }
                }
            })
            .state('app.bbspost', {
                url: "/bbs/:id",
                views: {
                    'menuContent': {
                        controller: 'BbsPostCtrl',
                        templateUrl: "templates/comments.html"
                    }
                }
            })
            .state('app.single', {
                url: "/courses/:courseId",
                views: {
                    'menuContent': {
                        templateUrl: "templates/comments.html",
                        controller: 'CommentsCtrl'
                    }
                }
            });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/scores');
    });

